0 pas bateau et pas touché
1 pas de bateau et touché
2 bateau et pas touché
3 bateau et touché



# Structure des fichiers

plateau
    - 1
        - A.txt
        - B.txt
        - ...
        - J.txt
    - 2
        - ...
    - ...
    - 10
        - ...
        
## SSH

- plateau/
- ...sh
- com/
    - ~~wait_con/~~
        - (c)ready.txt <- ip@/path/com
    - wait_reset/
        - ready.txt <- /path/plateau
        - (c)reponse.txt  <- /path/plateau
        - finish.txt <- result



