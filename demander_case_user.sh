#! /usr/bin/env bash

# Fonction pour demander à un joueur de placer ses navires. Paramètres : num du joueur.   
source ./getCase.sh
source ./updateCase.sh
source ./affiche_plateau.sh

demander_placement_navires() {
      joueur=$1
      echo "Joueur $joueur, veuillez placer vos navires sur le plateau."

      for i in {1..5}
      do
        affiche_plateau_joueur $plateau_h $plateau_w "$joueur" false
        echo "Veuillez placer un navire. Navire $i/5"
        read -p "Coordonnées : " coordonnees
        while true
        do
            colonne=${coordonnees:0:1}
            ligne=${coordonnees:1:1}
            etat=$(getCase $colonne $ligne $joueur)
            if [[ $colonne = [A-$plateau_w] ]] && [[ $ligne -ge 1 ]] && [[ $ligne -le $plateau_h ]] && [[ $etat = 0 ]]
            then
                #placer le navire sur le plateau
                nouveletat=2
                updateCase $colonne $ligne $joueur $nouveletat
                break
            else
                echo "Erreur : format de coordonnées incorrect ou emplacement déjà choisi. Veuillez choisir un emplacement vide et entrez des coordonnées valides (par exemple, A1)." > /dev/stderr
            fi
            read -p "Coordonnées (par exemple, A1) : " coordonnees
        done
      done
      
        affiche_plateau_joueur $plateau_h $plateau_w "$joueur" false
      # Demander à chaque joueur de placer ses navires
     #demander_placement_navires 1
      #demander_placement_navires 2
}

 
