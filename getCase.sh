#! /usr/bin/env bash

# Paramètre : getCase $colone(lettre) $ligne(chiffre) $n°Joueur(1/2)
# Return: Etat de la case pour le joueur : 0 / 1 / 2 / 3 (voir Readme.md)
function getCase {
	chemin=$(pwd)

	cd plateau/$2
	etatJ=$(cat $1.txt | cut -d":" -f$3)
	echo "$etatJ"

	cd $chemin
}
