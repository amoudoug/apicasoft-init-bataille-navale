#! /usr/bin/env bash

source ./updateCase.sh
source ./getCase.sh

# Paramètre : checkTir $colone(lettre) $ligne(chiffre) $n°Joueur sur qui on tir(1/2)
# Return: 0 : Raté | 1 : Touché | 2 : Déjà tiré
function checkTir {
	chemin=$(pwd)

	state=$(getCase $1 $2 $3)

	case $state in
		0) res=0;;
		1) res=2;;
		2) res=1;;
		3) res=2;;
		*) echo "Error: $state";;
	esac

	case $res in
		0) updateCase $1 $2 $3 1;;
		1) updateCase $1 $2 $3 3;;
		2) ;;
		*) echo "Error: $res";;
	esac

	echo "$res"
	cd $chemin
}
