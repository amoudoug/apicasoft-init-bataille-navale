#! /usr/bin/env bash

# Prends en paramètre le nombre de ligne nbr_ligne et de colonne nbr_colonne
function creer_plateau {
	nbr_colonne=$2
	nbr_ligne=$1 
	mkdir plateau
	cd plateau
	for i in $(seq 1 $nbr_ligne)
	do
		mkdir $i
		cd $i
		for y in $(eval echo {A..$nbr_colonne})
		do
			touch $y.txt
			echo "0:0">>$y.txt
		done
		cd ..
	done
	cd ..
}

