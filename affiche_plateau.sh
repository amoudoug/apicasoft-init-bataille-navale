#!/usr/bin/env bash

source getCase.sh

emptyLineLimits () {
    echo -n "   |  "
    for j_affiche_plateau in $(eval echo {A..$y})
    do
        echo -n "    "
    done
    echo -n "|"
    echo ""
}

# AFFICHE LE PLATEAU DE 1 JOUEUR SANS FILTRE
# $1 => taille en x
# $2 => taille en y
# $3 => joueur actuel 0 ou 1
# $4 => si on met un filtre ou non / true false
affiche_plateau_joueur () {
    x=$1
    y=$2
    joueur=$3
    filter=$4

    echo -n "       "
    for j_affiche_plateau in $(eval echo {A..$y})
    do
        echo -n "$j_affiche_plateau   "
    done
    echo ""

    echo -n "   +———"
    for j_affiche_plateau in $(eval echo {A..$y})
    do
        echo -n "————"
    done
    echo -ne "\b+"
    echo ""

    emptyLineLimits

    for i_affiche_plateau in $(seq 1 $x)
    do
        printf -v i_temp "%2d" $i_affiche_plateau
        echo -n "$i_temp |  "
        for j_affiche_plateau in $(eval echo {A..$y})
        do
            case="$(getCase $j_affiche_plateau $i_affiche_plateau $joueur)"
            if "$filter"
            then
                case $case in
                    0) echo -n " ?  ";;
                    1) echo -n " .  ";;
                    2) echo -n " ?  ";;
                    3) echo -n " X  ";;
                esac
            else
                case $case in
                    2) echo -n " $  ";;
                    3) echo -n " X  ";;
                    *) echo -n " .  ";;
                esac
            fi
        done
        echo "|"

        emptyLineLimits
    done

    echo -n "   +———"
    for j_affiche_plateau in $(eval echo {A..$y})
    do
        echo -n "————"
    done
    echo -ne "\b+"
    echo ""
}

# $1 => taille en x
# $2 => taille en y
# $3 => joueur actuel
# $4 => joeuur adverse
affiche_plateau () {
    x=$1
    y=$2
    joueur_actuel=$3
    joueur_adverse=$4

    echo "   VOTRE PLATEAU"
    echo "   —————————————————"
    affiche_plateau_joueur "$x" "$y" "$joueur_actuel" false

    echo ""

    echo "   PLATEAU ADVERSE"
    echo "   —————————————————"
    affiche_plateau_joueur "$x" "$y" "$joueur_adverse" true

    echo ""
    echo "   ?: Case inconnue    .: De l'eau"
    echo "   X: Bateau coulé     $: Vos bateaux"
    echo ""
}
