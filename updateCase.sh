#! /usr/bin/env bash

# Paramètre : updateCase $colone(lettre) $ligne(chiffre) $n°Joueur(1/2) $nouvelEtatCase(0 / 1 / 2 / 3)
# Return: none
function updateCase {

	chemin=$(pwd)

	cd plateau/$2
	numFile="$1.txt"

	if [ $3 -eq 1 ]; then
		etatJ=$(cat $numFile | cut -d":" -f2)
		echo "$4:$etatJ" > $numFile
	elif [ $3 -eq 2 ]; then
		etatJ=$(cat $numFile | cut -d":" -f1)
		echo "$etatJ:$4" > $numFile
	else echo "Error"
	fi

	cd $chemin
}
