#! /usr/bin/env bash
plateau_h=10 
plateau_w=J
bateau_restant1=5
bateau_restant2=5
source menu.sh
source creer_plateau.sh
source initialise_environnement.sh
source affiche_plateau.sh
source demander_case_user.sh
source getCase.sh
source updateCase.sh
source checkTir.sh
source verifier_case_user.sh

function main {
	initialisation
	creer_plateau $plateau_h $plateau_w
	menu
	clear
	demander_placement_navires 1
	clear
        demander_placement_navires 2
	clear
	while [ $bateau_restant1 -ne 0 ]&&[ $bateau_restant2 -ne 0 ]
	do
		affiche_plateau $plateau_h $plateau_w 1 2 
		case_vise="$(verif_placement_cases 1)"
		echo $case_vise
		colonne_vise=${case_vise:0:1}
		ligne_vise=${case_vise:1:1}
		etat=$(checkTir $colonne_vise $ligne_vise 2) #32
		if [ $etat -eq 1 ]
		then
			echo "Touché" 
			bateau_restant2=$(($bateau_restant2 - 1))
		else
			echo "Raté!" 
		fi
		echo "Il vous reste $bateau_restant1 bateaux et il reste $bateau_restant2 bateaux à l'adversaire"
		read -p "Appuyer sur n'importe quelle touche pour passer au tour suivant"
		clear
		affiche_plateau $plateau_h $plateau_w 2 1 
		case_vise=$(verif_placement_cases 2)
		echo "$case_vise"
                colonne_vise=${case_vise:0:1}
                ligne_vise=${case_vise:1:1}
                etat=$(checkTir $colonne_vise $ligne_vise 1)
                if [ $etat -eq 1 ]
                then
                        echo "Touché" 
			bateau_restant1=$(($bateau_restant1 - 1))
                else
                        echo "Raté!" 
               fi
		echo "Il vous reste $bateau_restant2 bateaux et il reste $bateau_restant1 bateaux à l'adversaire"
		read -p "Appuyer sur n'importe quelle touche pour passer au tour suivant"
                clear
	done
	if [ $bateau_restant1 -eq 0 ] && [ $bateau_restant2 -eq 0 ]
	then
		echo "Egalité mes petits veaux" | cowsay
	elif [ $bateau_restant1 -eq 0 ]
	then
		echo "Le joueur 2 a gagné meuhhhhhhHHHHHHH!!!!!" | cowsay
	else 
		echo "Le joueur 1  a gagné! meuhhhHHHHHHHH!!!!" | cowsay
	fi
}
main
